# mdl (MarkdownLint) Style Configuration

# include all the built-in rules by default
all

# custom parameters
rule 'MD013', line_length: 100
rule 'MD046', style: 'consistent'

# exclude specific rules
# exclude_rule 'MD000'
