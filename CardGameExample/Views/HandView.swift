//

import SwiftUI

//    struct Card {
//        var name: String
//    }

struct HandView: View {
    let cards = ["a", "b", "c", "d", "e"]
    var body: some View {
        ForEach(cards, id: \.self) { card in
            HStack {
                Text(card)
                Image(systemName: "heart")
            }
        }
    }
}

struct HandView_Previews: PreviewProvider {
    static var previews: some View {
        HandView()
    }
}
