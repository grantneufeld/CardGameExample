import SwiftUI

struct GameView: View {
    #if DEBUG // for ViewInspector
    // This is the simple way to make @State/@ObservedObject/etc. changes,
    // gesture updates, and other things that can change over a view’s lifetime,
    // accessible to ViewInspector.
    // There are other approaches that may be more appropriate to your code’s needs
    // (such as when working with asynchronous code, like Combine).
    // See the ViewInspector documentation for details.
    // https://github.com/nalexn/ViewInspector/blob/master/guide.md
    internal var didAppear: ((Self) -> Void)?
    #endif

    // This is a minimal, overly simplistic, sample view—
    // just so we have something to poke around with our tests.
    var body: some View {
        VStack {
            // 0:
            Section("Fun") {
                // 0:
                Text("Yes!")
            }
            // 1:
            List {
                // 0:
                HandView() // Our only custom sub-view in this view.
                // 1:
                Spacer()
                // 2:
                Text("Whatever")
            }
        }
        #if DEBUG // for ViewInspector
        // This needs the `didAppear` declaration above.
        .onAppear { self.didAppear?(self) }
        #endif
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
    }
}
