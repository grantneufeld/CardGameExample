import SwiftUI

struct MainMenuView: View {
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink("New Game") {
                    GameView()
                }
                Text("Test")
            }
        }
    }
}

struct MainMenuView_Previews: PreviewProvider {
    static var previews: some View {
        MainMenuView()
    }
}
