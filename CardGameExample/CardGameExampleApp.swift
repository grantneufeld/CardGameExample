import SwiftUI

@main
struct CardGameExampleApp: App {
    var body: some Scene {
        WindowGroup {
            MainMenuView()
        }
    }
}
