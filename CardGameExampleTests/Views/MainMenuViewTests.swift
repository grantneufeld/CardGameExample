@testable import CardGameExample
import ViewInspector
import XCTest

class MainMenuViewTests: XCTestCase {
    var sut: MainMenuView!

    override func setUpWithError() throws {
        sut = MainMenuView()
    }

    //    func testNewGameButton_exists() {
    //        XCTAssertNoThrow(try sut.inspect().find(button: "New Game"))
    //    }

    func testNewGameButton_hasLabelNewGame() throws {
        let text = try sut.inspect().find(text: "Test")
        XCTAssertEqual(try text.string(), "Test")
    }

    func testGameNavigationLink_exists() {
        XCTAssertNoThrow(try sut.inspect().find(navigationLink: "New Game"))
    }

    func testGameNavigationLink_linksToGameView() throws {
        let navLink = try sut.inspect().find(navigationLink: "New Game")
        XCTAssertNoThrow(try navLink.view(GameView.self))
    }
}

extension MainMenuView: Inspectable {}
