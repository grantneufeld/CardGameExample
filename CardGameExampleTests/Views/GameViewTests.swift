@testable import CardGameExample
import ViewInspector
import XCTest

class GameViewTests: XCTestCase {
    /// sut == System Under Test: the class/struct/etc. that we’re trying to test here
    private var sut: GameView!

    /// We initialize the `sut` the same way for every test.
    override func setUpWithError() throws {
        sut = GameView()
    }

    override func tearDownWithError() throws {
        sut = nil
        // Not really necessary, but I include this so SwiftLint won’t show an erroneous error 🤷:
        try super.tearDownWithError()
    }

    /// The most basic sort of test when using ViewInspector:
    /// Determine if the expected sub-view (in this case, a `HandView`) is present like we expect.
    /// This uses ViewInspector’s `find` mechanism, so we do not need to know which—if any—
    /// other sub-views might be wrapped around the view we’re looking for.
    func testHandView_exists() {
        XCTAssertNoThrow(try sut.inspect().find(HandView.self))
    }

    // In the current iteration of our GameView, it contains:
    //
    // * VStack
    //   * Section("Fun")
    //     * Text("Yes!")
    //   * List
    //     * HandView
    //     * Spacer
    //     * Text("Whatever")

    /// This is not how one would typically write such a test,
    /// but it serves here as an example of stepping through the hierarchy of views, one-by-one.
    /// Unlike the previous test which used `find`,
    /// here we rely on knowing the exact hierarchy of the sub-views within our view.
    func testSubViews_byHierarchy() throws {
        // `sutView` is the name I use for the `InspectableView` of our `sut`
        // (that we get from ViewInspector).
        let sutView = try sut.inspect().view(GameView.self)
        // The VStack is the “root” View within the GameView,
        // so it’s accessed as the first item off of our sut:
        let vStack = try sutView.vStack()
        // Sidenote:
        //   I’ve setup some of the indexes as `let` constants in the “Helpers” section below.
        //   Using constants instead of raw values is, of course, far preferable for readability
        //   (assuming clear names are used…).
        //   But it’s also a VERY important habit to get into when referencing views by index.
        //   Anytime you modify your View, you may end up shifting things around
        //   resulting in the old index values no longer being correct.
        //   It’s a lot easier to change those values in one place than try to deal with
        //   updating raw values spread across various tests 😖.
        //   So, all that just to point out that I have `listIndexInStack` here instead of `1`.
        // The List is at the top level within the VStack, at it’s given index (1, in this case).
        let list = try vStack.list(listIndexInStack)
        // Since we’ll be checking the hand in multiple tests,
        // I made a helper function to get it (the helper section is near the end of this file):
        let hand = try getHandView(sutView: sutView)
        // The values/objects we get back from most ViewInspector calls or not our actual Views,
        // they are wrapped in `InspectableView`.
        // So, we need to call `actualView()` if we want to access our view,
        // such as to get it’s instance values.
        let actualHandView = try hand.actualView()
        // Now, on our actual view, we can check it’s instance values, such as the `cards` array:
        XCTAssertFalse(actualHandView.cards.isEmpty)
        // Spacers are pretty boring, so we’ll just test that it exists:
        XCTAssertNoThrow(try list.spacer(spacerIndexInList))
        // Here, our `Text` view comes after the hand and spacer inside the List.
        // So, it’s the zero-counted third element, giving it an index of 2:
        let text = try list.text(textIndexInList)
        // `text` is not a `Text` but, rather, an `InspectableView<ViewType.Text>`.
        // So, the call to `string()` here is part of ViewInspector, not SwiftUI’s `Text` struct.
        let string = try text.string()
        // Finally, we just make sure the string in our Text view is “whatever” we expect it to be…
        XCTAssertEqual(string, "Whatever")
    }

    /// We get the hand sub-view (our custom `HandView`).
    /// Then we check that it has the expected sub-view (`ForEach`).
    /// 🛑 Do NOT actually do this in real tests.
    ///   Testing contents of a custom sub-view should be handled in the unit test for that View.
    func testHand_hasItsOwnSubView() throws {
        let sutView = try sut.inspect().view(GameView.self)
        let hand = try getHandView(sutView: sutView)
        XCTAssertNoThrow(try hand.forEach(0))
    }

    /// Some SwiftUI Views have stuff going on with labels, text, or other sub-elements,
    /// that we may want to check.
    /// This example looks at a `Section` View that has a header
    /// (because a string is passed in to the view: `Section("Fun")`).
    func testSectionFun_hasFunLabel() throws {
        let sutView = try sut.inspect().view(GameView.self)
        // GameView contains VStack contains Section at index 0:
        let section = try sutView.vStack().section(funSectionIndexInStack)
        // Section shows the text we passed in within a “header” view, which contains a Text view.
        // We call `string()` on that text view to get the string value it is showing.
        // (I use the raw `0` value here instead of a constant, for the text index
        //  within the header, because the text is always the first element
        //  in the header of a section when using SwiftUI’s default section labeling like this.)
        let sectionName = try section.header().text(0).string()
        XCTAssertEqual(sectionName, "Fun")
    }

    // MARK: - HELPERS

    // As stated above, it is very, very, useful to put all of
    // your sub-view indexing constants in one spot like this.
    // (Bonus points if you can come up with clear, meaningful, names 🏆)
    private let funSectionIndexInStack: Int = 0
    private let listIndexInStack: Int = 1
    private let handIndexInList: Int = 0
    private let spacerIndexInList: Int = 1
    private let textIndexInList: Int = 2

    /// Test Helper: finds the `HandView` within the `GameView`
    /// - Parameter sutView: The GameView wrapped in a ViewInspector `InspectableView`.
    /// - Returns: The HandView wrapped in a ViewInspector `InspectableView`.
    /// - Throws: Errors from ViewInspector if the view could not be found where expected.
    private func getHandView(
        sutView: InspectableView<ViewType.View<GameView>>
    ) throws -> InspectableView<ViewType.View<HandView>> {
        try sutView.vStack().list(listIndexInStack).view(HandView.self, handIndexInList)
    }
}

// In order for ViewInspector to be able to access a SwiftUI View,
// the `Inspectable` protocol needs to be assigned to it like this:
extension GameView: Inspectable {}
