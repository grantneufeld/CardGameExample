@testable import CardGameExample
import ViewInspector
import XCTest

class HandViewTests: XCTestCase {
    var sut: HandView!

    override func setUpWithError() throws {
        sut = HandView()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testCards_has5Cards() throws {
        let forEach = try sut.inspect().view(HandView.self).forEach()
        let cards = ["a", "b", "c", "d", "e"]
        for index in 0..<5 {
            let text = try forEach.hStack(index).text(0)
            XCTAssertEqual(try text.string(), cards[index])
            let image = try forEach.hStack(index).image(1)
            XCTAssertEqual(try image.actualImage().name(), "heart")
        }
    }
}

extension HandView: Inspectable {}
