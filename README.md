# Card Game Example

An example project for using the [ViewInspector](https://github.com/nalexn/ViewInspector)
library/package to support unit testing SwiftUI views.

This came out of a live coding session to introduce folks to ViewInspector.
The code isn’t intended to have any actual use beyond illustrating how to unit test SwiftUI.

## Tools & Libraries Used

* [ViewInspector](https://github.com/nalexn/ViewInspector)
* [SwiftLint](https://github.com/realm/SwiftLint)
* [Markdown lint](https://github.com/markdownlint/markdownlint) (mdl)

## ViewInspector Resources

### ViewInspector Documentation

* [ViewInspector Inspection Guide](https://github.com/nalexn/ViewInspector/blob/master/guide.md)
* [ViewInspector readiness list](https://github.com/nalexn/ViewInspector/blob/master/readiness.md)
* [ViewInspector validating Styles](https://github.com/nalexn/ViewInspector/blob/master/guide_styles.md)
* [ViewInspector testing Gestures](https://github.com/nalexn/ViewInspector/blob/master/guide_gestures.md)

### Example Code

* [Clean Architecture for SwiftUI + Combine](https://github.com/nalexn/clean-architecture-swiftui)

### Other Resources

* [TDD Academy: iOS micro testing with ViewInspector, introduced by creator Alexey Naumov](https://player.twitch.tv/?parent=meta.tag&video=815405793)
* [SwiftUI Testing With ViewInspector for iOS](https://www.raywenderlich.com/30227776-swiftui-testing-with-viewinspector-for-ios)
* [How to Test SwiftUI Views Containing @State in ViewInspector](https://betterprogramming.pub/swiftui-simple-trick-to-test-views-containing-state-in-viewinspector-d98092ee558e)
* [Test-Driven iOS Development with Swift - Fourth Edition](https://subscription-rc.packtpub.com/book/web_development/9781803232485),
  by Dominik Hauser

## Contributors

* [Grant Neufeld](https://twitter.com/grant)

## License

Copyright ©2022 Grant Neufeld.

MIT License

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
